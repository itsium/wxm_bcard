class Bcard < ActiveRecord::Base
  has_one :weiapp, as: :app, dependent: :destroy
  has_one :user, :through => :weiapp
  has_many :contact_fields, as: :contactable, dependent: :destroy
  accepts_nested_attributes_for :contact_fields

  # attr_accessor :user_id
  has_attached_file :photo, :styles => { :original => "300x300#", :thumb => "100x100#" }, :default_url => "/images/:style/photo_missing.png"
  validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/

  def display_name
    "BusinessCard of #{name}"
  end

  def display_description
    display_name
  end

  def identifier
    :bcard
  end

  def as_json(options=nil)
    item = super({ only: [
        :id, :name, :company, :jobtitle, :theme, :vcardurl
    ] }.merge(options || {}))
    item.merge({
      fields: self.contact_fields,
      photo: self.photo.url(:thumb)
    })
  end

end
