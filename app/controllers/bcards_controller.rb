class BcardsController < InheritedResources::Base
    before_filter :check_user, only: [ :create, :update, :edit ]
    layout "mobile", only: [:mobileshow]

    def mobileshow
        @bcard = Weiapp.where(appuid: params[:appuid]).first().app
    end

    def show
        @bcard = Bcard.find(params[:id])
    end

    def create
        @bcard = Bcard.new(permitted_params[:bcard])
        @bcard.user = current_user
        create!
    end

    def update
        @bcard = Bcard.find(params[:id])
        @bcard.user = current_user
        update!
    end

    def destroy
        destroy! { a_url }
    end

#  name             :string(255)
#  value            :string(255)
#  ftype            :integer
#  enumerize :ftype, in: [:phone, :email, :mobile, :address, :website, :fax, :social_network, :weixin, :weibo, :qq]

    def new
        @bcard = Bcard.new
        @bcard.contact_fields.build(ftype: :mobile, name: t(:mobile))
        @bcard.contact_fields.build(ftype: :phone, name: t(:phone))
        @bcard.contact_fields.build(ftype: :fax, name: t(:fax))
        @bcard.contact_fields.build(ftype: :email, name: t(:email))
        @bcard.contact_fields.build(ftype: :website, name: t(:website))
        @bcard.contact_fields.build(ftype: :address, name: t(:address))
        @bcard.contact_fields.build(ftype: :weixin, name: t(:weixin))
        @bcard.contact_fields.build(ftype: :weibo, name: t(:weibo))
        @bcard.contact_fields.build(ftype: :qq, name: t(:qq))
    end

    def edit
        @bcard = Bcard.find(params[:id])
        @qr_link = "#{root_url}#{@bcard.weiapp.appuid}"
        @qr_for_link = gen_qrcode(@qr_link)
    end

    protected


    def permitted_params
        # TODO SET CORRECT FILTER
        params.permit!
    end
end
