$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "wxm_bcard/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "wxm_bcard"
  s.version     = WxmBcard::VERSION
  s.authors     = ["itsium.cn"]
  s.email       = ["dev@itsium.cn"]
  s.homepage    = "http://itsium.cn"
  s.summary     = "Business card extension for wxm."
  s.description = "A virtual social network based business card feature."

  s.files = Dir["{app,config,db,lib}/**/*", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0.5"
  s.add_dependency "paperclip"
  s.add_dependency "jpbuilder"

  s.add_development_dependency "sqlite3"
end

