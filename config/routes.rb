Rails.application.routes.draw do
  resources :bcards
  get 'bcapp/:appuid', to: 'bcards#mobileshow'
  get 'bcards/preview/:id' => 'bcards#preview'
end
