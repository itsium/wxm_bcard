class AddAttachmentPhotoToBcards < ActiveRecord::Migration
  def self.up
    change_table :bcards do |t|
      t.attachment :photo
    end
  end

  def self.down
    drop_attached_file :bcards, :photo
  end
end
